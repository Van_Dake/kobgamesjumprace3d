﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControls : MonoBehaviour
{
    public Rigidbody rb;
    public Animator anim;
    public float moveSpeed = 7f;
    public float turnSpeed = 10f;
    public float minTurnValue = .3f;
    public int maxJumpPoses;
    public float minJumpVelocity = 8;     // jump no lower than this much
    public float maxJumpVelocity = 60f;     // jump and fall no faster than this much
    public float longJumpVelocity = 50f;    // how fast we should fall to qualify for long jump

    bool longJump;
    float horizontal, vertical;
    void FixedUpdate()
    {
        HandleMovement();
        rb.AddForce(Physics.gravity * rb.mass);

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxJumpVelocity);
        if (rb.velocity.y < -longJumpVelocity && !longJump)
        {
            longJump = true;
        }
    }

    public virtual void HandleMovement()
    {
        if (!GameMaster.Instance.start) return;
        HandleInput();
        SetDirection();
        SetRotation();
    }

    private void HandleInput()
    {
        if (InputControls.Instance.IsInputDown())
            vertical = 1;
        else
        {
            vertical = 0;
            rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
        }

        horizontal = InputControls.Instance.DeltaDrag.x;
        if (Mathf.Abs(horizontal) < minTurnValue)
            horizontal = 0f;
    }

    private void SetRotation()
    {
        float turn = horizontal * turnSpeed;
        Vector3 torque = new Vector3(0f, turn, 0f);
        rb.AddTorque(torque);
    }

    private void SetDirection()
    {
        Vector3 direction = transform.forward * moveSpeed * vertical;
        float step = moveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, step);
    }

    public virtual void Jump(float multiplier)
    {
        if (longJump)
        {
            multiplier *= 2f; // double the value when long jump
            if (transform.CompareTag("Player"))
            {
                GameMaster.Instance.SpawnLongJump();
            }
        }
        float currentVelocity = rb.velocity.y;
        float newFall = Mathf.Clamp(currentVelocity, minJumpVelocity, maxJumpVelocity);
        
        rb.velocity = Vector3.zero;
        rb.AddForce(transform.up * multiplier * newFall, ForceMode.Impulse);
        int jumpPose = Random.Range(0, maxJumpPoses - 1);
        longJump = false;

        if (!anim) return; //debugging only. change ASAP

        anim.SetTrigger("Land");
        anim.SetInteger("JumpPose", jumpPose);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Water") && this.CompareTag("Player"))
        {
            GameMaster.Instance.SpawnGameOver();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameMaster : MonoBehaviour
{
    #region Singleton "Instance"
    private static GameMaster _instance;
    public static GameMaster Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        GetAllPlatformsInOrder();
    }
    #endregion

    public bool start;

    [Header("Progess Bar")]
    public int levelNumber;
    public TextMeshProUGUI[] levelIndicators; // 0 should be current, 1 should be next
    public UI_Displays mainUI;
    public Slider progressBar;
    public Transform platformHolder;
    public List<Platform_Bounce> platforms;

    [HideInInspector] public int platformIndex;

    [Header("Placements")]
    public Transform UI_playerPlacement;
    public AI_Movement[] AI;
    
    [HideInInspector] public bool finalPlacements;
    private bool initTextNums;
    private TextMeshProUGUI uI_playerNumber;
    private TextMeshProUGUI uI_AI1;
    private TextMeshProUGUI uI_AI2;
    
    [Header("Prompts")]
    public GameObject perfectPrompt;
    public GameObject goodPrompt;
    public GameObject longJumpPrompt;
    public GameObject gameOverPrompt;

    private void Start()
    {
        levelIndicators[0].text = levelNumber.ToString();
        levelIndicators[1].text = (levelNumber + 1).ToString();
        InvokeRepeating(nameof(UpdatePlacements), 0f, 0.1f);
    }

    public void UpdatePlacements()
    {
        if (!initTextNums)
        {
            uI_playerNumber = UI_playerPlacement.GetChild(0).GetComponent<TextMeshProUGUI>();
            uI_AI1 = AI[0].UI_PlacementPanel.GetChild(0).GetComponent<TextMeshProUGUI>();
            uI_AI2 = AI[1].UI_PlacementPanel.GetChild(0).GetComponent<TextMeshProUGUI>();
            initTextNums = true;
        }

        for (int i = 0; i < AI.Length; i++)
        {
            if (platformIndex > AI[i].targetPlatIndex - 1)
            {
                if(UI_playerPlacement.GetSiblingIndex() > AI[i].UI_PlacementPanel.GetSiblingIndex())
                    UI_playerPlacement.SetSiblingIndex(AI[i].UI_PlacementPanel.GetSiblingIndex());

            }
            else if (platformIndex < AI[i].targetPlatIndex - 1)
            {
                if(UI_playerPlacement.GetSiblingIndex() < AI[i].UI_PlacementPanel.GetSiblingIndex())
                    UI_playerPlacement.SetSiblingIndex(AI[i].UI_PlacementPanel.GetSiblingIndex());
            }
        }

        uI_playerNumber.text = (UI_playerPlacement.GetSiblingIndex()+1).ToString();
        uI_AI1.text = (AI[0].UI_PlacementPanel.GetSiblingIndex()+1).ToString();
        uI_AI2.text = (AI[1].UI_PlacementPanel.GetSiblingIndex()+1).ToString();
    }


    private void GetAllPlatformsInOrder()
    {
        platforms = platformHolder.GetComponentsInChildren<Platform_Bounce>().ToList();
        for (int i = 0; i < platforms.Count; i++)
        {
            platforms[i].platformID = i;
        }
        progressBar.maxValue = platforms.Count;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void SetBouncedPlatform(int platformID)
    {
        platformIndex = platformID;
        UpdateUI();
    }

    public void UpdateUI()
    {
        progressBar.value = platformIndex;
    }

    public void SpawnPerfect()
    {
        if (!start) return;
        GameObject prompt = Instantiate(perfectPrompt);
        Destroy(prompt, 2f);
    }
    public void SpawnGood()
    {
        if (!start) return;
        GameObject prompt = Instantiate(goodPrompt);
        Destroy(prompt, 2f);
    }
    public void SpawnLongJump()
    {
        if (!start) return;
        GameObject prompt = Instantiate(longJumpPrompt);
        Destroy(prompt, 2f);
    }
    public void SpawnGameOver()
    {
        start = false;
        Instantiate(gameOverPrompt);
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(levelNumber);
    }
}

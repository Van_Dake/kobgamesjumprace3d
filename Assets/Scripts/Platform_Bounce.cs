﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_Bounce : MonoBehaviour
{
    public int platformID;
    public bool movesSideways;
    public bool destructible;
    public bool sinking;
    public GameObject debris;
    public float maxJump = 50f;
    public Animator vfxAnimator;
    Animator anim;

    [Header("Multipliers")]
    public Transform center;
    public Vector3 centerOffset;
    public float perfectRange, perfectMult; 
    public float goodRange, goodMult;

    [Header("Debris")]
    public Rigidbody[] rigidbodies;
    public float explosionRadius;
    public float maxExplosionForce, minExplosionForce;
    public float destroyDelay = 2f;

    private void Start()
    {
        anim = GetComponent<Animator>();
        
        if(movesSideways)
            anim.SetBool("MoveSideways", movesSideways);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("AI"))
        {
            if (vfxAnimator != null)
            {
                vfxAnimator.SetTrigger("Bounce");
            }

            //handle multipliers
            float jump = maxJump;
            if (center)
            {
                float distance = Vector3.Distance(collision.GetContact(0).point, center.position + centerOffset);
                
                if (distance <= perfectRange)
                {
                    jump *= perfectMult;
                    if(collision.gameObject.CompareTag("Player"))
                        GameMaster.Instance.SpawnPerfect();
                }
                else if (distance <= goodRange)
                {
                    jump *= goodMult;
                    if(collision.gameObject.CompareTag("Player"))
                        GameMaster.Instance.SpawnGood();
                }
            }
            
            collision.gameObject.GetComponent<CharacterControls>().Jump(jump); //change to depending on where the point of collision is

            if (!GameMaster.Instance.start)
                return;

            if (collision.gameObject.CompareTag("Player") && !sinking)
                GameMaster.Instance.SetBouncedPlatform(platformID);

            if (destructible)
            {
                Destroy(GetComponent<Collider>());
                Invoke(nameof(BecomeDebris), 0.05f);
            }
            else if(sinking)
            {
                Sink();
            }
        }
    }

    private void Sink()
    {
        anim.SetTrigger("Sink");
    }

    private void BecomeDebris()
    {
        foreach (var item in rigidbodies)
        {
            item.isKinematic = false;
            float explosion = Random.Range(minExplosionForce, maxExplosionForce);
            item.AddExplosionForce(explosion, transform.position, explosionRadius);
        }
        Destroy(gameObject, destroyDelay);
    }

    //private void SpawnDebris()
    //{
    //    if (debris)
    //    {
    //        GameObject debs = Instantiate(debris, transform.position, transform.rotation, null);
    //        if (debs.GetComponent<Platform_Bounce>())
    //        {
    //            debs.GetComponent<Platform_Bounce>().platformID = this.platformID;
    //            GameMaster.Instance.platforms[platformID] = debs.GetComponent<Platform_Bounce>();
    //        }
            
    //    }
    //    Destroy(gameObject);
    //    Destroy(vfxAnimator.gameObject, 3f);
    //}
}

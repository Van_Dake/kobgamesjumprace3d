﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChangeColor : MonoBehaviour
{
    public enum Type { Static, Sides, Destructible }
    public Type type;
    public MeshRenderer mr;

    [Header("Static Platform")]
    public Color s_Center;
    public Color s_Inner;
    public Color s_Outer;
    
    [Header("Sides Platform")]
    public Color sd_Center;
    public Color sd_Inner;
    public Color sd_Outer;
    
    [Header("Destructible Platform")]
    public Color d_Center;
    public Color d_Inner;
    public Color d_Outer;

    void Start()
    {
        if (!mr)
            mr = transform.Find("Target").GetComponent<MeshRenderer>();

        switch (type)
        {
            case Type.Static:
                mr.materials[0].SetColor("_Color", s_Outer);
                mr.materials[1].SetColor("_Color", s_Inner); 
                mr.materials[2].SetColor("_Color", s_Center);
                break;
            case Type.Sides:
                mr.materials[0].SetColor("_Color", sd_Outer); 
                mr.materials[1].SetColor("_Color", sd_Inner);
                mr.materials[2].SetColor("_Color", sd_Outer);
                break;
            case Type.Destructible:
                mr.materials[0].SetColor("_Color", d_Center); 
                mr.materials[1].SetColor("_Color", d_Inner);
                mr.materials[2].SetColor("_Color", d_Outer);
                break;
            default:
                break;
        }

        
    }
}


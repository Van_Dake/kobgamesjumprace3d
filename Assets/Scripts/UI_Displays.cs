﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Displays : MonoBehaviour
{
    public GameObject tapToStart;

    private void Update()
    {
        if(Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            tapToStart.SetActive(false);
            GameMaster.Instance.start = true;
        }
    }
}

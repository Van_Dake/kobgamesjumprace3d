﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputControls : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static InputControls Instance;
    public delegate void InputEvent();
    public event InputEvent OnInputDown;
    public event InputEvent OnInputUp;
    [Header("Settings")]
    public float DragSensitivity = 100f;
    [Space]

    [HideInInspector] public Vector2 DeltaDrag;
    [HideInInspector] public Vector2 Drag;
    private bool isInputDown;
    private Vector3 mousePosition => (Input.mousePosition * (2 * 0.5f));
    private Vector3 inputDownPosition;
    private Vector3 lastInputPosition;


    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    //private void OnDestroy()
    //{
    //    if (Instance == this)
    //        Instance = null;
    //}


    public void OnPointerDown(PointerEventData eventData)
    {
        isInputDown = true;
        inputDownPosition = lastInputPosition = mousePosition;
        ResetValues();
        OnInputDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isInputDown = false;
        ResetValues();
        OnInputUp?.Invoke();
    }

    private void ResetValues()
    {
        DeltaDrag = Drag = Vector2.zero;
    }

    public bool IsInputDown() => isInputDown;

    private void FixedUpdate()
    {
        if (isInputDown)
        {
            Drag = (mousePosition - inputDownPosition);// * DragSensitivity;
            Drag = Drag.normalized;
            DeltaDrag = (mousePosition - lastInputPosition); //* DragSensitivity;
            DeltaDrag = DeltaDrag.normalized;
            lastInputPosition = mousePosition;
        }
    }
}

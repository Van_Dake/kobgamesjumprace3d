﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyAlpha : MonoBehaviour
{
    public Transform cam;
    public SpriteRenderer sr;
    public float distanceToFade;
    float distance;
    Color newAlpha;
    void Update()
    {
        if (cam.position.y > transform.position.y)
        {
            distance = (cam.position.y - transform.position.y) / 100;
            if (distance >= 0 && distance <= 1f)
            {
                float alpha = distance;
                newAlpha = new Color(1f, 1f, 1f, alpha);
                sr.color = newAlpha;
            }
            
        }
    }
}

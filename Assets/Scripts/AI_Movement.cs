﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class AI_Movement : CharacterControls
{
    [Header("Platform Targetting")]
    public float targetRadiusError = 1.5f;
    public int targetPlatIndex = 0;
    public Transform UI_PlacementPanel;
    [SerializeField] Transform finishPlatform;
    Transform targetPlatform;
    Vector3 targetPos;
    Vector3 direction;
    Vector3 errorOffset;
    bool startMove;

    private void Start()
    {
        if (!finishPlatform)
            finishPlatform = FindObjectOfType<FinishLine>().transform;
    }

    public override void HandleMovement()
    {
        if (!GameMaster.Instance.start && !startMove)
            return;

        if (targetPlatIndex < GameMaster.Instance.platforms.Count)
        {
            if (GameMaster.Instance.platforms[targetPlatIndex] == null)
            {
                targetPlatIndex++;
                return;
            }
            targetPlatform = GameMaster.Instance.platforms[targetPlatIndex].transform;
        }
        else
        {
            targetPlatform = finishPlatform;
        }
        
        
        
        direction = targetPlatform.position - transform.position;
        Vector3 flatDirection = new Vector3(direction.x, 0f, direction.z);

        if (!startMove) return;

        float step = moveSpeed * Time.deltaTime;
        targetPos = transform.position + flatDirection + errorOffset;
        transform.position = Vector3.MoveTowards(transform.position, targetPos, step);

        HandleRotation(targetPos);
    }

    public override void Jump(float multiplier)
    {
        UpdateError();
        
        base.Jump(multiplier);
        if (GameMaster.Instance.start)
        {
            targetPlatIndex++;
            startMove = true;
        }
    }

    private void HandleRotation(Vector3 position)
    {
        transform.LookAt(position);
    }

    private void UpdateError()
    {
        float randomX = Random.Range(-targetRadiusError, targetRadiusError);
        float randomZ = Random.Range(-targetRadiusError, targetRadiusError);
        errorOffset = new Vector3(randomX, 0f, randomZ);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    public GameObject finishPrompt;
    public UI_Recap uiRecap;
    public Animator playerAnim;
    public ParticleSystem[] confetti;

    bool notFirst;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            for (int i = 0; i < confetti.Length; i++)
            {
                confetti[i].Play();
            }

            GameMaster.Instance.progressBar.value = GameMaster.Instance.progressBar.maxValue;

            if (!notFirst)
                GameMaster.Instance.SetBouncedPlatform(GameMaster.Instance.platforms.Count);

            uiRecap.CopyPositions();
            GameMaster.Instance.UpdatePlacements();

            playerAnim.SetBool("Finish", true);
            Instantiate(finishPrompt);
            GameMaster.Instance.start = false;
            GameMaster.Instance.finalPlacements = true;
            //Destroy(prompt, 3f);
        }
        else
        {
            notFirst = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFan : MonoBehaviour
{
    public Rigidbody fan;
    public float torque;

    private void FixedUpdate()
    {
        fan.AddRelativeTorque(0f, 0f, torque, ForceMode.Acceleration);
    }
}

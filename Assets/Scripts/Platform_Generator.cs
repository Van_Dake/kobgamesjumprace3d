﻿using PathCreation;
using UnityEngine;

namespace PathCreation.Examples
{
    [ExecuteInEditMode]
    public class Platform_Generator : PathSceneTool
    {
        public LineRenderer lr;
        public GameObject startPrefab;
        public GameObject[] prefabs;
        public GameObject holder;
        public float spacing = 3;
        public float lineSpacing = 1f;

        const float minSpacing = .1f;
        const float minLineSpacing = .1f;
        void Generate()
        {
            if (pathCreator != null && startPrefab != null && holder != null)
            {
                DestroyObjects();

                VertexPath path = pathCreator.path;

                spacing = Mathf.Max(minSpacing, spacing);
                float dst = 0;
                while (dst < path.length)
                {
                    Vector3 point = path.GetPointAtDistance(dst);
                    Quaternion rot = path.GetRotationAtDistance(dst);
                    rot.x = 0f;
                    rot.z = 0f;
                    if(dst == 0)
                        Instantiate(startPrefab, point, rot, holder.transform);
                    else if(prefabs.Length > 0)
                    {
                        int randIndex = Random.Range(0, prefabs.Length - 1);
                        if(!prefabs[randIndex].CompareTag("Fan"))
                            Instantiate(prefabs[randIndex], point, rot, holder.transform);
                        else //insert a fan between two platforms
                        {
                            Instantiate(startPrefab, point, rot, holder.transform);

                            point = path.GetPointAtDistance(dst - spacing *.5f); //reassign point
                            Instantiate(prefabs[randIndex], point, rot, holder.transform);
                        }

                    }

                    dst += spacing;
                }

                lineSpacing = Mathf.Max(minLineSpacing, lineSpacing);
                float lineDist = 0;
                lr.positionCount = 0;
                int lineIndex = 0;
                while(lineDist < path.length)
                {
                    lr.positionCount++;
                    Vector3 linePoint = path.GetPointAtDistance(lineDist);
                    lr.SetPosition(lineIndex, linePoint);
                    lineIndex++;
                    lineDist += lineSpacing;
                }
            }
        }

        void DestroyObjects()
        {
            int numChildren = holder.transform.childCount;
            for (int i = numChildren - 1; i >= 0; i--)
            {
                DestroyImmediate(holder.transform.GetChild(i).gameObject, false);
            }
        }

        protected override void PathUpdated()
        {
            if (pathCreator != null)
            {
                Generate();
            }
        }
    }
}
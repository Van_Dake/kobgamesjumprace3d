﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_Recap : MonoBehaviour
{
    public Transform playerPlacePanel;
    public Animator animator;

    [Header("Copy Index of Placement Holder")]
    public Transform copyPlayerPanel;
    public Transform[] otherPanels; 
    public Transform[] copyPanel;
    public TextMeshProUGUI[] finalPositions;

    [Header("Holders")]
    public Transform fill;
    
    private void Start()
    {
        InvokeRepeating(nameof(CopyPositions), 0f, 0.001f);
    }

    private void Update()
    {
        if (GameMaster.Instance.finalPlacements)
        {
            CancelInvoke();
            FinalizePositions();
        }
    }

    private void FinalizePositions()
    {
        for (int i = 0; i < fill.childCount; i++)
        {
            finalPositions[i].text = fill.GetChild(i).GetComponent<TextMeshProUGUI>().text;
        }
        animator.SetTrigger("Recap");
    }

    public void CopyPositions()
    {
        playerPlacePanel.SetSiblingIndex(copyPlayerPanel.GetSiblingIndex());
        for (int i = 0; i < otherPanels.Length; i++)
        {
            otherPanels[i].SetSiblingIndex(copyPanel[i].GetSiblingIndex());
        }
    }
}

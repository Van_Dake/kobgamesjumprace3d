﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Platform_Numbering : MonoBehaviour
{
    public Platform_Bounce p_Bounce;
    public TextMeshProUGUI text;

    private void Start()
    {
        Invoke(nameof(InitializeNumber), 0.01f);
    }

    private void InitializeNumber()
    {
        int number = Mathf.Abs(GameMaster.Instance.platforms.Count - p_Bounce.platformID);
        text.text = number.ToString();
    }
}

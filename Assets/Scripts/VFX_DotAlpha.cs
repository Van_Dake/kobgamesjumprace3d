﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_DotAlpha : MonoBehaviour
{
    public SpriteRenderer sr;
    public Color color;
    public float detectRadius;
    public LayerMask detectFor;
    float currentDist;
    float alpha = 0f;

    Transform player;
    RaycastHit hit;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        InvokeRepeating(nameof(ControlAlpha), 0f, 0.01f);
    }

    private void ControlAlpha()
    {
        currentDist = Vector3.Distance(transform.position, player.position);
        if (currentDist > detectRadius) 
        {
            sr.color = Color.clear;
            return;
        }
        alpha = Mathf.Abs(1 - (currentDist / detectRadius));
        color.a = alpha;
        sr.color = color;
    }

    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawWireSphere(transform.position, detectRadius);
    //}
}

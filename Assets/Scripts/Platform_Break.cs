﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_Break : MonoBehaviour
{
    public Rigidbody[] rigidbodies;
    public float explosionRadius;
    public float maxExplosionForce, minExplosionForce;
    public float destroyDelay = 2f;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in rigidbodies)
        {
            float explosion = Random.Range(minExplosionForce, maxExplosionForce);
            item.AddExplosionForce(explosion, transform.position, explosionRadius);
        }
        Destroy(gameObject, destroyDelay);
    }
}

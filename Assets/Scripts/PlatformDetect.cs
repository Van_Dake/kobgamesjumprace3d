﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDetect : MonoBehaviour
{
    public LineRenderer lr;
    public LayerMask whatIsPlatform;
    public float maxDetectionRange = 200f;
    public Vector3 detectStartOffset;
    public Transform dot;
    public List<Color> colors;

    RaycastHit hit;

    private void Update()
    {
        lr.SetPosition(0, transform.position);
        dot.position = hit.point;
    }

    private void FixedUpdate()
    {
        DetectPlatform();
    }

    private void DetectPlatform()
    {
        if(Physics.Raycast(transform.position + detectStartOffset, -transform.up, out hit , maxDetectionRange, whatIsPlatform))
        {
            lr.SetPosition(1, hit.point);
            Color detect = colors[0];
            detect.a = 0.5f;
            lr.startColor = detect;
            lr.endColor = colors[0];
            
        }
        else
        {
            Vector3 linePos = new Vector3(transform.position.x, 0f, transform.position.z);
            lr.SetPosition(1, linePos);
            Color detect = colors[1];
            detect.a = 0.5f;
            lr.startColor = detect;
            lr.endColor = colors[1];
        }
    }
}
